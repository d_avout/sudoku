package com.radek.sudoku.validator;

import com.google.common.collect.Table;

public interface SudokuSolutionValidator  {
    void validate(Table<Integer, Integer, Integer> sudokuTable);
}
