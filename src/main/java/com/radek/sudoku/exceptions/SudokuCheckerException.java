package com.radek.sudoku.exceptions;

public class SudokuCheckerException extends RuntimeException {


    public SudokuCheckerException(String message){
        super(message);
    }

}
