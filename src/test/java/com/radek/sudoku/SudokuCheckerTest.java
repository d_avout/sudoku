package com.radek.sudoku;

import com.radek.sudoku.parser.Sudoku9x9FileParser;
import com.radek.sudoku.utils.TestUtils;
import com.radek.sudoku.validator.Sudoku9x9SolutionValidator;
import org.junit.Test;
import picocli.CommandLine;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


public class SudokuCheckerTest {

    @Test
    public void returns0ExitCodeOnValidSolution() {
        SudokuChecker sudokuChecker = new SudokuChecker(new Sudoku9x9FileParser(),new Sudoku9x9SolutionValidator());
        CommandLine cmd = new CommandLine(sudokuChecker);

        int exitCode = cmd.execute(TestUtils.getTestFile("validator/valid_solution.txt").getAbsolutePath());

        assertThat(exitCode, is(0));
    }

    @Test
    public void returns1ExitCodeOnInvalidSolution() {
        SudokuChecker sudokuChecker = new SudokuChecker(new Sudoku9x9FileParser(),new Sudoku9x9SolutionValidator());
        CommandLine cmd = new CommandLine(sudokuChecker);

        int exitCode = cmd.execute(TestUtils.getTestFile("validator/invalid_solution_duplicates_1.txt").getAbsolutePath());

        assertThat(exitCode, is(1));
    }
}
