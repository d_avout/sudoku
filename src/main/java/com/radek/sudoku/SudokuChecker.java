package com.radek.sudoku;

import com.google.common.collect.Table;
import com.radek.sudoku.exceptions.SudokuCheckerException;
import com.radek.sudoku.parser.Sudoku9x9FileParser;
import com.radek.sudoku.parser.SudokuFileParser;
import com.radek.sudoku.validator.Sudoku9x9SolutionValidator;
import com.radek.sudoku.validator.SudokuSolutionValidator;
import picocli.CommandLine;

import java.io.File;
import java.util.concurrent.Callable;

@CommandLine.Command(name="validate", mixinStandardHelpOptions = true,description = "Validates Sudoku solution")
public class SudokuChecker implements Callable<Integer> {

    @CommandLine.Parameters(index = "0", description = "File with Sudoku solution")
    private File file;

    private SudokuFileParser parser;
    private SudokuSolutionValidator validator;


    public SudokuChecker(SudokuFileParser parser, SudokuSolutionValidator validator) {
        this.parser = parser;
        this.validator = validator;
    }

    public static void main(String[] args) {
        SudokuChecker runnable = new SudokuChecker(new Sudoku9x9FileParser(),new Sudoku9x9SolutionValidator());
        CommandLine commandLine = new CommandLine(runnable);
        int exitCode = commandLine.execute(args);
        System.out.println(exitCode);
    }

    @Override
    public Integer call() {
        try {
            Table<Integer, Integer, Integer> table = parser.parseFile(file);
            validator.validate(table);
            System.out.println("Valid solution");
            return 0;
        } catch (SudokuCheckerException e) {
            System.out.println("Invalid solution");
            System.out.println(e.getMessage());
            return 1;
        } catch (Exception e) {
            System.out.println("Unexpected error");
            System.out.println(e.getMessage());
            return 1;
        }
    }

}
