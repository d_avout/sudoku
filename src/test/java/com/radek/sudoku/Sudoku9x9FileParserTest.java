package com.radek.sudoku;

import com.google.common.collect.Table;
import com.radek.sudoku.exceptions.SudokuValidationException;
import com.radek.sudoku.parser.Sudoku9x9FileParser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

import static com.radek.sudoku.utils.TestUtils.getTestFile;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnit4.class)
public class Sudoku9x9FileParserTest {

    private Sudoku9x9FileParser objectUnderTest = new Sudoku9x9FileParser();

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void canParseFile() {
        File testFile = getTestFile("parser/correct_input.txt");
        Table<Integer, Integer, Integer> result = objectUnderTest.parseFile(testFile);

        assertThat(result.columnKeySet().size(),is(9));
        for (int rowNo = 1; rowNo < 10; rowNo++) {
            for (int colNo = 1; colNo < 10; colNo++) {
                Integer value = result.get(rowNo, colNo);
                assertThat(value, is(colNo));
            }
        }
    }

    @Test
    public void canParseFileWithSpaces() {
        File testFile = getTestFile("parser/correct_input_spaces_allowed.txt");
        Table<Integer, Integer, Integer> result = objectUnderTest.parseFile(testFile);

        assertThat(result.columnKeySet().size(),is(9));
        for (int rowNo = 1; rowNo < 10; rowNo++) {
            for (int colNo = 1; colNo < 10; colNo++) {
                Integer value = result.get(rowNo, colNo);
                assertThat(value, is(colNo));
            }
        }
    }
    @Test
    public void throwsSudokuValidationExceptionWhenAnyValueIsNotBetween1and9_1(){
        File testFile = getTestFile("parser/incorrect_input_outside_boundary_1.txt");

        expectedEx.expect(SudokuValidationException.class);
        expectedEx.expectMessage("Issue found at row 5 and column: 6. Details: Value must be between 1 and 9");

        objectUnderTest.parseFile(testFile);
    }

    @Test
    public void throwsSudokuValidationExceptionWhenAnyValueIsNotBetween1and9_2(){
        File testFile = getTestFile("parser/incorrect_input_outside_boundary_2.txt");

        expectedEx.expect(SudokuValidationException.class);
        expectedEx.expectMessage("Issue found at row 5 and column: 9. Details: Value must be between 1 and 9");

        objectUnderTest.parseFile(testFile);
    }

    @Test
    public void throwsSudokuValidationExceptionWhenThereIsANoIntegerValue(){
        File testFile = getTestFile("parser/incorrect_input_not_integer_value.txt");

        expectedEx.expect(SudokuValidationException.class);
        expectedEx.expectMessage("Issue found at row 4 and column: 4. Details: Value is not a number");

        objectUnderTest.parseFile(testFile);
    }

    @Test
    public void throwsSudokuValidationExceptionWhenThereIsAnEmptyValue(){
        File testFile = getTestFile("parser/incorrect_input_empty_value.txt");

        expectedEx.expect(SudokuValidationException.class);
        expectedEx.expectMessage("Issue found at row 7 and column: 6. Details: Value is not a number");

        objectUnderTest.parseFile(testFile);
    }

    @Test
    public void throwsSudokuValidationExceptionWhenThereIsLessThan9RowsInFile(){
        File testFile = getTestFile("parser/incorrect_input_less_than_9_rows.txt");

        expectedEx.expect(SudokuValidationException.class);
        expectedEx.expectMessage("Number of rows in the file is not equal to 9");

        objectUnderTest.parseFile(testFile);
    }

    @Test
    public void throwsSudokuValidationExceptionWhenThereIsLessThan9ColumnsInFile(){
        File testFile = getTestFile("parser/incorrect_input_less_than_9_columns.txt");

        expectedEx.expect(SudokuValidationException.class);
        expectedEx.expectMessage("Number of columns in the file is not equal to 9");

        objectUnderTest.parseFile(testFile);
    }

    @Test
    public void throwsSudokuValidationExceptionWhenThereIsMoreThan9RowsInFile(){
        File testFile = getTestFile("parser/incorrect_input_more_than_9_rows.txt");

        expectedEx.expect(SudokuValidationException.class);
        expectedEx.expectMessage("Number of rows in the file is not equal to 9");

        objectUnderTest.parseFile(testFile);
    }

    @Test
    public void throwsSudokuValidationExceptionWhenThereIsMoreThan9ColumnsInFile(){
        File testFile = getTestFile("parser/incorrect_input_more_than_9_columns.txt");

        expectedEx.expect(SudokuValidationException.class);
        expectedEx.expectMessage("Number of columns in the file is not equal to 9");

        objectUnderTest.parseFile(testFile);
    }

    @Test
    public void throwsSudokuValidationExceptionIfFileNotFound(){
        expectedEx.expect(SudokuValidationException.class);
        expectedEx.expectMessage("Path to input file: totally_incorrect_path\\file.txt is incorrect");

        objectUnderTest.parseFile(new File("totally_incorrect_path/file.txt"));
    }
}
