package com.radek.sudoku.exceptions;

public class SudokuValidationException extends SudokuCheckerException {

    private static final String ERROR_MESSAGE = "Issue found at row %d and column: %d. Details: %s";
    public SudokuValidationException(Integer row, Integer column, String message) {
        this(String.format(ERROR_MESSAGE,row,column,message));
    }

    public SudokuValidationException(String message) {
        super(message);
    }
}
