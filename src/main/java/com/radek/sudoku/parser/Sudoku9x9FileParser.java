package com.radek.sudoku.parser;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.radek.sudoku.exceptions.SudokuValidationException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.stream.Collectors;

public class Sudoku9x9FileParser implements SudokuFileParser {

    @Override
    public Table<Integer, Integer, Integer> parseFile(File file){
        try (BufferedReader reader = new BufferedReader(getFile(file))) {
            List<String> lines = retrieveLines(reader);
            return buildTable(lines);
        } catch (SudokuValidationException e) {
            throw e;
        } catch (Exception e) {
            throw new SudokuValidationException("Unexpected error. Could not read the input file.");
        }
    }

    private List<String> retrieveLines(BufferedReader reader) {
        List<String> lines = reader.lines().filter(line -> !line.isEmpty()).collect(Collectors.toList());
        if (lines.size() != 9) {
            throw new SudokuValidationException("Number of rows in the file is not equal to 9");
        }
        return lines;
    }

    private Table<Integer, Integer, Integer> buildTable(List<String> lines) {
        Table<Integer, Integer, Integer> table = HashBasedTable.create();
        for (int rowIndex = 0; rowIndex < lines.size(); rowIndex++) {
            String[] columns = retrieveColumns(lines, rowIndex);
            for (int colIndex = 0; colIndex < columns.length; colIndex++) {
                table.put(rowIndex + 1, colIndex + 1, retrieveValue(columns, rowIndex, colIndex));
            }
        }
        return table;
    }

    private String[] retrieveColumns(List<String> lines, int rowIndex) {
        String[] columns = lines.get(rowIndex).split(",");
        if (columns.length != 9) {
            throw new SudokuValidationException("Number of columns in the file is not equal to 9");
        }
        return columns;
    }

    private int retrieveValue(String[] line, int rowIndex, int columnIndex) {
        try {
            int value = Integer.parseInt(line[columnIndex].trim());
            if (isOutsideAllowedBoundary(value)) {
                throw new SudokuValidationException(rowIndex + 1, columnIndex + 1, "Value must be between 1 and 9");
            } else {
                return value;
            }
        } catch (NumberFormatException e) {
            throw new SudokuValidationException(rowIndex + 1, columnIndex + 1, "Value is not a number");
        }
    }

    private FileReader getFile(File file) {
        try {
            return new FileReader(file);
        } catch (FileNotFoundException e) {
            throw new SudokuValidationException("Path to input file: " + file.getPath() + " is incorrect");
        }
    }

    private boolean isOutsideAllowedBoundary(int value) {
        return value < 1 || value > 9;
    }
}
