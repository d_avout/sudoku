package com.radek.sudoku;

import com.google.common.collect.Table;
import com.radek.sudoku.exceptions.SudokuValidationException;
import com.radek.sudoku.parser.Sudoku9x9FileParser;
import com.radek.sudoku.validator.Sudoku9x9SolutionValidator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

import static com.radek.sudoku.utils.TestUtils.getTestFile;

@RunWith(JUnit4.class)
public class Sudoku9x9SolutionValidatorTest {
    private Sudoku9x9FileParser parser = new Sudoku9x9FileParser();
    private Sudoku9x9SolutionValidator objectUnderTest = new Sudoku9x9SolutionValidator();

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void canVerifySudoku(){
        File testFile = getTestFile("validator/valid_solution.txt");
        Table<Integer, Integer, Integer> table = parser.parseFile(testFile);

        objectUnderTest.validate(table);
    }

    @Test
    public void throwsSudokuValidationExceptionWhenBlockContainsDuplicateValues_1(){
        expectedEx.expect(SudokuValidationException.class);
        expectedEx.expectMessage("Row number 4 has more than one number of 6");

        File testFile = getTestFile("validator/invalid_solution_duplicates_1.txt");
        Table<Integer, Integer, Integer> table = parser.parseFile(testFile);

        objectUnderTest.validate(table);
    }

    @Test
    public void throwsSudokuValidationExceptionWhenBlockContainsDuplicateValues_2(){
        expectedEx.expect(SudokuValidationException.class);
        expectedEx.expectMessage("Row number 1 has more than one number of 2");

        File testFile = getTestFile("validator/invalid_solution_duplicates_2.txt");
        Table<Integer, Integer, Integer> table = parser.parseFile(testFile);

        objectUnderTest.validate(table);
    }

    @Test
    public void throwsSudokuValidationExceptionWhenSubsquareContainsDuplicates_1(){
        expectedEx.expect(SudokuValidationException.class);
        expectedEx.expectMessage("Issue found at row 2 and column: 1. Details: Subsquare has more than one number of 4");

        File testFile = getTestFile("validator/invalid_solution_duplicates_in_square_1.txt");
        Table<Integer, Integer, Integer> table = parser.parseFile(testFile);

        objectUnderTest.validate(table);
    }

    @Test
    public void throwsSudokuValidationExceptionWhenSubsquareContainsDuplicates_2(){
        expectedEx.expect(SudokuValidationException.class);
        expectedEx.expectMessage("Issue found at row 3 and column: 1. Details: Subsquare has more than one number of 9");

        File testFile = getTestFile("validator/invalid_solution_duplicates_in_square_2.txt");
        Table<Integer, Integer, Integer> table = parser.parseFile(testFile);

        objectUnderTest.validate(table);
    }
}
