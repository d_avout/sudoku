package com.radek.sudoku.parser;

import com.google.common.collect.Table;

import java.io.File;

public interface SudokuFileParser {
    Table<Integer, Integer, Integer> parseFile(File file);
}
