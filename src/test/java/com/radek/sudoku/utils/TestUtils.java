package com.radek.sudoku.utils;

import java.io.File;

public class TestUtils {
    public static File getTestFile(String path) {
        ClassLoader classLoader = TestUtils.class.getClassLoader();
        return new File(classLoader.getResource(path).getFile());
    }
}
