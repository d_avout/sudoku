@echo off

java -version 2> NUL
if errorlevel 1 goto java_missing

:execute
set JAR_NAME=%~dp0/@@NAME_PLACEHOLDER@@
set MAIN_CLASS=com.radek.sudoku.SudokuChecker

java -cp "%JAR_NAME%" "%MAIN_CLASS%" %*

goto end




:java_missing
@echo ERROR: Java could not be found

:end

