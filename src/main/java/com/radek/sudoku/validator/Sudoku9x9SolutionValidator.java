package com.radek.sudoku.validator;

import com.google.common.collect.Table;
import com.radek.sudoku.exceptions.SudokuValidationException;

import java.util.HashSet;

public class Sudoku9x9SolutionValidator implements SudokuSolutionValidator{

    private static final int GRID_SIZE = 9;

    @Override
    public void validate(Table<Integer, Integer, Integer> sudokuTable) {
        validateNumberUniquenessOnRows(sudokuTable);
        validateNumberUniquenessOnColumns(sudokuTable);
        validateSubsquares(sudokuTable);
    }

    private void validateNumberUniquenessOnRows(Table<Integer, Integer, Integer> sudokuTable) {
        for (int rowNo = 1; rowNo < GRID_SIZE + 1; rowNo++) {
            HashSet<Integer> set = new HashSet<>();
            for (int colNo = 1; colNo < GRID_SIZE + 1; colNo++) {
                Integer value = sudokuTable.get(rowNo, colNo);
                if (!set.add(value)) {
                    throw new SudokuValidationException("Row number " + rowNo + " has more than one number of " + value);
                }
            }
        }
    }

    private void validateNumberUniquenessOnColumns(Table<Integer, Integer, Integer> sudokuTable) {
        for (int colNo = 1; colNo < GRID_SIZE + 1; colNo++) {
            HashSet<Integer> set = new HashSet<>();
            for (int rowNo = 1; rowNo < GRID_SIZE + 1; rowNo++) {
                Integer value = sudokuTable.get(rowNo, colNo);
                if (!set.add(value)) {
                    throw new SudokuValidationException("Column number " + colNo + " has more than one number of " + value);
                }
            }
        }
    }

    private void validateSubsquares(Table<Integer, Integer, Integer> sudokuTable) {
        for (int squareStartRow = 1; squareStartRow < GRID_SIZE + 1; squareStartRow = squareStartRow + 3) {
            for (int squareStartCol = 1; squareStartCol < GRID_SIZE + 1; squareStartCol = squareStartCol + 3) {
                HashSet<Integer> set = new HashSet<>();
                for (int row = squareStartRow; row < squareStartRow + 3; row++) {
                    for (int col = squareStartCol; col < squareStartCol + 3; col++) {
                        Integer value = sudokuTable.get(row, col);
                        if (!set.add(value)) {
                            throw new SudokuValidationException(row, col, "Subsquare has more than one number of " + value);
                        }
                    }
                }
            }
        }
    }

}
